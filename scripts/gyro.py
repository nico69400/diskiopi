#!/usr/bin/python
import smbus
import math
import os
 
# Register
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c
 
def read_byte(reg):
    return bus.read_byte_data(address, reg)
 
def read_word(reg):
    h = bus.read_byte_data(address, reg)
    l = bus.read_byte_data(address, reg+1)
    value = (h << 8) + l
    return value
 
def read_word_2c(reg):
    val = read_word(reg)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val
 
def dist(a,b):
    return math.sqrt((a*a)+(b*b))
 
def get_y_rotation(x,y,z):
    radians = math.atan2(x, dist(y,z))
    return -math.degrees(radians)
 
def get_x_rotation(x,y,z):
    radians = math.atan2(y, dist(x,z))
    return math.degrees(radians)
 

while True: 
        bus = smbus.SMBus(1) # bus = smbus.SMBus(0) fuer Revision 1
        address = 0x68       # via i2cdetect
 
# Aktivieren, um das Modul ansprechen zu koennen
        bus.write_byte_data(address, power_mgmt_1, 0)
 
 
        gyroskop_xout = read_word_2c(0x43)
        gyroskop_yout = read_word_2c(0x45)
        gyroskop_zout = read_word_2c(0x47)
 
        beschleunigung_xout = read_word_2c(0x3b)
        beschleunigung_yout = read_word_2c(0x3d)
        beschleunigung_zout = read_word_2c(0x3f)
 
        beschleunigung_xout_skaliert = beschleunigung_xout / 16384.0
        beschleunigung_yout_skaliert = beschleunigung_yout / 16384.0
        beschleunigung_zout_skaliert = beschleunigung_zout / 16384.0
 
        #position =  get_y_rotation(beschleunigung_xout_skaliert, beschleunigung_yout_skaliert, beschleunigung_zout_skaliert)
        positionx = beschleunigung_xout
        #positiony = beschleunigung_yout
        #positionz = beschleunigung_zout
    
    
        if positionx > 350 and positionx < 800 :
            #print("gauche")
            os.system('xrandr -o left')
            os.system("xinput set-prop 'ILITEK Multi-Touch-V3000' 'Coordinate Transformation Matrix' 0 -1 1 1 0 0 0 0 1")
    
        if positionx < -90 and positionx > -1000:
            #print("droite")
            os.system('xrandr -o right')
            os.system("xinput set-prop 'ILITEK Multi-Touch-V3000' 'Coordinate Transformation Matrix' 0 1 0 -1 0 1 0 0 1")
        
        if positionx > 14500 and positionx < 17000 :  
            #print("normal")
            os.system('xrandr -o normal')
            os.system("xinput set-prop 'ILITEK Multi-Touch-V3000' 'Coordinate Transformation Matrix' 1 0 0 0 1 0 0 0 1")

        if positionx > -16000 and positionx < -10000 :
            #print("a lenvers")
            os.system('xrandr -o inverted')
            os.system("xinput set-prop 'ILITEK Multi-Touch-V3000' 'Coordinate Transformation Matrix' -1 0 1 0 -1 1 0 0 1")

        os.system('sleep 1')


        #a commenter
        #print(position) 
        #test
