#!/bin/sh

mkdir ~/temp
cd ~/temp
wget http://plippo.de/dwl/twofing/twofing-0.1.2.tar.gz
sudo apt-get update
sudo apt-get install build-essential libx11-dev libxtst-dev libxi-dev x11proto-randr-dev libxrandr-dev -y

tar -xvzf twofing-0.1.2.tar.gz
cd twofing-0.1.2
make

sudo cp twofing /usr/bin/

sudo touch /etc/udev/rules.d/70-touchscreen-ilitek.rules
sudo echo "SUBSYSTEMS=="usb",ACTION=="add",KERNEL=="event*",ATTRS{idVendor}=="222a",ATTRS{idProduct}=="0001",SYMLINK+="twofingtouch",RUN+="/bin/chmod a+r /dev/twofingtouch"
KERNEL=="event*",ATTRS{name}=="ILITEK Multi-Touch-V3000",SYMLINK+="twofingtouch",RUN+="/bin/chmod a+r /dev/twofingtouch"" > /etc/udev/rules.d/70-touchscreen-ilitek.rules

sudo apt install xserver-xorg-input-evdev xinput-calibrator -y

sudo echo 'Section "InputClass"
    Identifier "calibration"
    Driver "evdev"
    MatchProduct "ILITEK Multi-Touch-V3000"
    MatchDevicePath "/dev/input/event*"
    Option "Emulate3Buttons" "True"
    Option "EmulateThirdButton" "1"
    Option "EmulateThirdButtonTimeout" "750"
    Option "EmulateThirdButtonMoveThreshold" "30"
EndSection' > /usr/share/X11/xorg.conf.d/90-touchinput.conf

sudo echo 'ACTION=="add", KERNEL=="event*", SUBSYSTEM=="input", TAG+="systemd", , ENV{SYSTEMD_ALIAS}+="/sys/subsystem/input/devices/$env{ID_SERIAL}"' > /etc/udev/rules.d/99-input-tagging.rules

mkdir ~/.config/autostart
echo '[Desktop Entry]
Type=Application
Name=Twofing autostart
Comment=Start the twofing program
Exec=twofing --wait
StartupNotify=false' > ~/.config/autostart/twofing.desktop
